# README #

This repository holds Zigbee fixture user scripts and code.

Note that infrastrucutre scripts are *not* housed here but are instead stored in the repo at hhttps://bitbucket.org/haxlab-public/haxlab-zigbee-fixture.

### Overview ###
These scripts do the following:

* BulbPowerOff.sh - Powers off the attached Zigbee Ikea bulb used as a sample device under test.
* BulbPowerOn.sh - Powers on the attached Zigbee Ikea bulb used as a sample device under test.
* BulbPowerToggle.sh - Toggles power to the attached Zigbee Ikea bulb to put it in pairing mode.

