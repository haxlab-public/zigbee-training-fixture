#!/bin/bash
# BulbPowerToggle.py - toggle power to Ikea bulb under test via canakit USB relay board to do pairing/factory reset.
#

# Turn off the power relay.
python ~/git/base-tools/relay/relay.py 4 -c
sleep 0.5

# Toggle power relay six times.
python ~/git/base-tools/relay/relay.py 4 -ts 0.5
sleep 0.5
python ~/git/base-tools/relay/relay.py 4 -ts 0.5
sleep 0.5
python ~/git/base-tools/relay/relay.py 4 -ts 0.5
sleep 0.5
python ~/git/base-tools/relay/relay.py 4 -ts 0.5
sleep 0.5
python ~/git/base-tools/relay/relay.py 4 -ts 0.5
sleep 0.5
python ~/git/base-tools/relay/relay.py 4 -ts 0.5
sleep 0.5

# Turn power on and leave on.
python ~/git/base-tools/relay/relay.py 4 -s
