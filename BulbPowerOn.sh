#!/bin/bash
# BulbPowerOn.py - turn on Ikea bulb under test via canakit USB relay board.
#

# Turn on the power relay.
python ~/git/base-tools/relay/relay.py 4 -s
