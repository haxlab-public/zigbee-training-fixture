#!/bin/bash
# BulbPowerOff.py - turn off Ikea bulb under test via canakit USB relay board.
#

# Turn off the power relay.
python ~/git/base-tools/relay/relay.py 4 -c
